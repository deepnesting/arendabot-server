package paymentSystem

import (
	"context"

	"github.com/deepnesting/arendabot-server/domain"
	"go.uber.org/zap"
)

type UsecaseImp struct {
	*zap.Logger
}

type Config struct{}

func NewUsecase(logger *zap.Logger) Usecase {
	return &UsecaseImp{Logger: logger}
}

func (uc *UsecaseImp) FetchTransactions(ctx context.Context) ([]domain.Transaction, error) {
	panic("unimplemented")
}
