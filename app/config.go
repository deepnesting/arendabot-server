package app

import (
	"fmt"

	"github.com/deepnesting/arendabot-server/domain/payment"

	"github.com/deepnesting/arendabot-server/domain/user"

	"github.com/deepnesting/arendabot-server/domain/wallet"

	"github.com/deepnesting/arendabot-server/domain/auth"

	"github.com/deepnesting/arendabot-server/domain/car"

	"github.com/deepnesting/arendabot-server/domain/driver"

	"github.com/deepnesting/arendabot-server/domain/offer"

	"github.com/deepnesting/arendabot-server/domain/shift"

	"github.com/deepnesting/arendabot-server/domain/stat"

	"github.com/deepnesting/arendabot-server/domain/setting"

	"github.com/gobuffalo/packr/v2"
	"gopkg.in/yaml.v2"
)

// Config is main app config
type Config struct {
	AppName string `yaml:"appName"`
	Env     string
	DB      struct {
		DSN string `yaml:"dsn"`
	} `yaml:"db"`
	JWTSecret string `yaml:"jwtSecret"`
	Upload    struct {
		Server string
		Key    string
		Secret string
	}

	Payment payment.Config

	User user.Config

	Wallet wallet.Config

	Auth auth.Config

	Car car.Config

	Driver driver.Config

	Offer offer.Config

	Shift shift.Config

	Stat stat.Config

	Setting setting.Config
}

// LoadConfig load config from fs, or pakred config
func LoadConfig(env string) (Config, error) {
	if env == "" {
		env = "local"
	}

	box := packr.New("conf", "./conf")

	s, err := box.FindString("conf/config.yml")
	if err != nil {
		return Config{}, err
	}

	var configs []Config

	err = yaml.Unmarshal([]byte(s), &configs)
	if err != nil {
		return Config{}, err
	}

	for _, cfg := range configs {
		if cfg.Env == env {
			return cfg, nil
		}
	}
	return Config{}, fmt.Errorf("env config not found")
}
