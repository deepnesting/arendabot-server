module github.com/deepnesting/arendabot-server

go 1.13

require (
	github.com/asdine/storm v2.1.2+incompatible
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/go-telegram-bot-api/telegram-bot-api v4.6.4+incompatible
	github.com/gobuffalo/packr/v2 v2.7.1
	github.com/labstack/echo/v4 v4.1.11
	github.com/nbutton23/zxcvbn-go v0.0.0-20180912185939-ae427f1e4c1d
	github.com/robfig/cron/v3 v3.0.0
	github.com/zhuharev/qiwi v1.0.0
	github.com/zhuharev/tamework v0.1.0
	go.etcd.io/bbolt v1.3.3 // indirect
	go.uber.org/zap v1.13.0
	gopkg.in/yaml.v2 v2.2.7
)
