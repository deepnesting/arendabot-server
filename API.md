# arendabot-server

## Namespaces

- [payment](#payment)
- [user](#user)
- [wallet](#wallet)
- [auth](#auth)
- [car](#car)
- [driver](#driver)
- [offer](#offer)
- [shift](#shift)
- [stat](#stat)
- [setting](#setting)
### payment
- [transactionsForNotification](#payment.transactionsForNotification)
- [setTransactionNotificationSent](#payment.setTransactionNotificationSent)
- [updateUserBalances](#payment.updateUserBalances)
- [byId](#payment.byId)
- [list](#payment.list)
- [listByUserId](#payment.listByUserId)
- [create](#payment.create)
- [update](#payment.update)
- [transferRealMoneyToAgents](#payment.transferRealMoneyToAgents)

#### payment.transactionsForNotification



Rest endpoint: `POST  /payments/transactionsForNotification`

##### Params

##### Return
- `transactions` (type: `[]domain.Transaction`)

#### payment.setTransactionNotificationSent



Rest endpoint: `POST  /payments/setTransactionNotificationSent`

##### Params
- `transactionID` (type: `int`)

##### Return

#### payment.updateUserBalances



Rest endpoint: `POST  /payments/updateUserBalances`

##### Params

##### Return

#### payment.byId



Rest endpoint: `GET  /payments/:paymentSystemId`


##### Params
- `id` (type: `int`)

##### Return
- `paymentSystem` (type: `*domain.PaymentSystem`)

#### payment.list



Rest endpoint: `POST  /payments/list`

##### Params

##### Return
- `paymentSystems` (type: `[]domain.PaymentSystem`)

#### payment.listByUserId



Rest endpoint: `POST  /payments/listByUserId`

##### Params
- `userId` (type: `int`)

##### Return
- `paymentSystems` (type: `[]domain.PaymentSystem`)

#### payment.create



Rest endpoint: `POST  /payments/create`

##### Params
- `paymentSystem` (type: `*domain.PaymentSystem`)

##### Return
- `paymentSystemId` (type: `int`)

#### payment.update



Rest endpoint: `POST  /payments/update`

##### Params
- `paymentSystem` (type: `*domain.PaymentSystem`)

##### Return

#### payment.transferRealMoneyToAgents



Rest endpoint: `POST  /payments/transferRealMoneyToAgents`

##### Params

##### Return
- `result` (type: `string`)


### user
- [searchFleets](#user.searchFleets)
- [autocompleteCity](#user.autocompleteCity)
- [addCity](#user.addCity)
- [updateCity](#user.updateCity)
- [deleteCity](#user.deleteCity)
- [cities](#user.cities)
- [list](#user.list)
- [byId](#user.byId)
- [byRole](#user.byRole)
- [byPhone](#user.byPhone)
- [byCityId](#user.byCityId)
- [byUsername](#user.byUsername)
- [update](#user.update)
- [create](#user.create)

#### user.searchFleets



Rest endpoint: `POST  /users/searchFleets`

##### Params
- `cityId` (type: `int`)
- `wages` (type: `bool`)
- `lease` (type: `bool`)
- `freeCars` (type: `bool`)

##### Return

#### user.autocompleteCity



Rest endpoint: `POST  /users/autocompleteCity`

##### Params
- `q` (type: `string`)

##### Return
- `result` (type: `[]domain.Autocomplete`)

#### user.addCity



Rest endpoint: `POST  /users/addCity`

##### Params
- `city` (type: `*domain.City`)

##### Return
- `cityId` (type: `int`)

#### user.updateCity



Rest endpoint: `POST  /users/updateCity`

##### Params
- `city` (type: `*domain.City`)

##### Return

#### user.deleteCity



Rest endpoint: `POST  /users/deleteCity`

##### Params
- `cityId` (type: `int`)

##### Return

#### user.cities



Rest endpoint: `POST  /users/cities`

##### Params

##### Return
- `cities` (type: `[]domain.City`)

#### user.list



Rest endpoint: `POST  /users/list`

##### Params

##### Return
- `users` (type: `[]domain.User`)

#### user.byId



Rest endpoint: `GET  /users/:userId`


##### Params
- `id` (type: `int`)

##### Return
- `user` (type: `*domain.User`)

#### user.byRole



Rest endpoint: `POST  /users/byRole`

##### Params
- `role` (type: `int`)

##### Return
- `users` (type: `[]domain.User`)

#### user.byPhone



Rest endpoint: `POST  /users/byPhone`

##### Params
- `phone` (type: `string`)

##### Return
- `user` (type: `*domain.User`)

#### user.byCityId



Rest endpoint: `POST  /users/byCityId`

##### Params
- `cityId` (type: `int`)

##### Return
- `user` (type: `[]domain.User`)

#### user.byUsername



Rest endpoint: `POST  /users/byUsername`

##### Params
- `username` (type: `string`)

##### Return
- `user` (type: `*domain.User`)

#### user.update



Rest endpoint: `POST  /users/update`

##### Params
- `user` (type: `*domain.User`)

##### Return

#### user.create



Rest endpoint: `POST  /users/create`

##### Params
- `user` (type: `*domain.User`)

##### Return
- `userId` (type: `int`)


### wallet
- [userTransactions](#wallet.userTransactions)
- [transactions](#wallet.transactions)
- [createTransaction](#wallet.createTransaction)

#### wallet.userTransactions

Возвращает транзакции пользователя

Rest endpoint: `POST  /wallet/userTransactions`

##### Params
- `userId` (type: `int`)

##### Return
- `transactions` (type: `[]domain.Transaction`)

#### wallet.transactions

Все транзакции (для админки)

Rest endpoint: `POST  /wallet/transactions`

##### Params

##### Return
- `transactions` (type: `[]domain.Transaction`)

#### wallet.createTransaction

создает транзакцию

Rest endpoint: `POST  /wallet/createTransaction`

##### Params
- `transaction` (type: `*domain.Transaction`)

##### Return
- `transactionId` (type: `int`)


### auth
- [me](#auth.me)
- [login](#auth.login)
- [sendCode](#auth.sendCode)

#### auth.me

Возвращает текущего пользователя (определяет по переданному токену).

Rest endpoint: `POST  /auth/me`

##### Params

##### Return
- `user` (type: `*domain.User`)

#### auth.login



Rest endpoint: `POST  /auth/login`

##### Params
- `phone` (type: `string`)
- `password` (type: `string`)

##### Return
- `token` (type: `string`)

#### auth.sendCode



Rest endpoint: `POST  /auth/sendCode`

##### Params

##### Return


### car
- [list](#car.list)
- [create](#car.create)
- [update](#car.update)
- [byFleetId](#car.byFleetId)
- [byId](#car.byId)

#### car.list



Rest endpoint: `POST  /cars/list`

##### Params

##### Return
- `cars` (type: `[]domain.Car`)

#### car.create



Rest endpoint: `POST  /cars/create`

##### Params
- `car` (type: `*domain.Car`)

##### Return
- `carId` (type: `int`)

#### car.update



Rest endpoint: `POST  /cars/update`

##### Params
- `car` (type: `*domain.Car`)

##### Return

#### car.byFleetId



Rest endpoint: `POST  /cars/byFleetId`

##### Params
- `fleetId` (type: `int`)

##### Return
- `cars` (type: `[]domain.Car`)

#### car.byId



Rest endpoint: `GET  /cars/:carId`


##### Params
- `id` (type: `int`)

##### Return
- `car` (type: `*domain.Car`)


### driver
- [list](#driver.list)
- [create](#driver.create)
- [update](#driver.update)
- [byAgentId](#driver.byAgentId)
- [byId](#driver.byId)

#### driver.list



Rest endpoint: `POST  /drivers/list`

##### Params

##### Return
- `drivers` (type: `[]domain.Driver`)

#### driver.create



Rest endpoint: `POST  /drivers/create`

##### Params
- `driver` (type: `*domain.Driver`)

##### Return
- `driverId` (type: `int`)

#### driver.update



Rest endpoint: `POST  /drivers/update`

##### Params
- `driver` (type: `*domain.Driver`)

##### Return

#### driver.byAgentId



Rest endpoint: `POST  /drivers/byAgentId`

##### Params
- `agentId` (type: `int`)

##### Return
- `drivers` (type: `[]domain.Driver`)

#### driver.byId



Rest endpoint: `GET  /drivers/:driverId`


##### Params
- `id` (type: `int`)

##### Return
- `driver` (type: `*domain.Driver`)


### offer
- [notify](#offer.notify)
- [list](#offer.list)
- [create](#offer.create)
- [update](#offer.update)
- [delete](#offer.delete)
- [byAgentId](#offer.byAgentId)
- [byFleetId](#offer.byFleetId)
- [byDriverId](#offer.byDriverId)
- [byId](#offer.byId)

#### offer.notify



Rest endpoint: `POST  /offers/notify`

##### Params

##### Return

#### offer.list



Rest endpoint: `POST  /offers/list`

##### Params

##### Return
- `offers` (type: `[]domain.Offer`)

#### offer.create



Rest endpoint: `POST  /offers/create`

##### Params
- `offer` (type: `*domain.Offer`)

##### Return
- `offerId` (type: `int`)

#### offer.update



Rest endpoint: `POST  /offers/update`

##### Params
- `offer` (type: `*domain.Offer`)

##### Return

#### offer.delete



Rest endpoint: `POST  /offers/delete`

##### Params
- `offerId` (type: `int`)

##### Return

#### offer.byAgentId



Rest endpoint: `POST  /offers/byAgentId`

##### Params
- `agentId` (type: `int`)

##### Return
- `offers` (type: `[]domain.Offer`)

#### offer.byFleetId



Rest endpoint: `POST  /offers/byFleetId`

##### Params
- `fleetId` (type: `int`)

##### Return
- `offers` (type: `[]domain.Offer`)

#### offer.byDriverId



Rest endpoint: `POST  /offers/byDriverId`

##### Params
- `fleetId` (type: `int`)

##### Return
- `offers` (type: `[]domain.Offer`)

#### offer.byId



Rest endpoint: `GET  /offers/:offerId`


##### Params
- `id` (type: `int`)

##### Return
- `offer` (type: `*domain.Offer`)


### shift
- [byID](#shift.byID)
- [rateShift](#shift.rateShift)
- [byDriverID](#shift.byDriverID)
- [byOfferID](#shift.byOfferID)
- [setRating](#shift.setRating)
- [list](#shift.list)
- [update](#shift.update)
- [create](#shift.create)
- [delete](#shift.delete)

#### shift.byID



Rest endpoint: `POST  /shifts/byID`

##### Params
- `id` (type: `int`)

##### Return
- `shift` (type: `*domain.Shift`)

#### shift.rateShift



Rest endpoint: `POST  /shifts/rateShift`

##### Params
- `shiftID` (type: `int`)
- `rating` (type: `int`)
- `comment` (type: `string`)

##### Return

#### shift.byDriverID



Rest endpoint: `POST  /shifts/byDriverID`

##### Params
- `driverID` (type: `int`)

##### Return
- `shifts` (type: `[]domain.Shift`)

#### shift.byOfferID



Rest endpoint: `POST  /shifts/byOfferID`

##### Params
- `offerID` (type: `int`)

##### Return
- `shifts` (type: `[]domain.Shift`)

#### shift.setRating



Rest endpoint: `POST  /shifts/setRating`

##### Params
- `shiftID` (type: `int`)
- `rating` (type: `int`)

##### Return

#### shift.list



Rest endpoint: `POST  /shifts/list`

##### Params

##### Return
- `shifts` (type: `[]domain.Shift`)

#### shift.update



Rest endpoint: `POST  /shifts/update`

##### Params
- `shift` (type: `*domain.Shift`)

##### Return

#### shift.create



Rest endpoint: `POST  /shifts/create`

##### Params
- `shift` (type: `*domain.Shift`)

##### Return
- `shiftId` (type: `int`)

#### shift.delete



Rest endpoint: `POST  /shifts/delete`

##### Params
- `shiftId` (type: `int`)

##### Return


### stat
- [sendCarsNotify](#stat.sendCarsNotify)
- [sendShiftNotify](#stat.sendShiftNotify)
- [list](#stat.list)

#### stat.sendCarsNotify



Rest endpoint: `POST  /stats/sendCarsNotify`

##### Params

##### Return
- `result` (type: `string`)

#### stat.sendShiftNotify



Rest endpoint: `POST  /stats/sendShiftNotify`

##### Params

##### Return
- `result` (type: `string`)

#### stat.list



Rest endpoint: `POST  /stats/list`

##### Params

##### Return
- `items` (type: `[]domain.StatsItem`)


### setting
- [list](#setting.list)
- [update](#setting.update)
- [create](#setting.create)

#### setting.list



Rest endpoint: `POST  /settings/list`

##### Params

##### Return
- `settings` (type: `[]domain.Setting`)

#### setting.update



Rest endpoint: `POST  /settings/update`

##### Params
- `setting` (type: `*domain.Setting`)

##### Return

#### setting.create



Rest endpoint: `POST  /settings/create`

##### Params
- `setting` (type: `*domain.Setting`)

##### Return
- `settingId` (type: `int`)




## Models
### user
- `id` (type: `int`)
- `name` (type: `string`)
- `info` (type: `string`)
- `address` (type: `string`)
- `workingHours` (type: `string`)
- `role` (type: `domain.UserRole`)
- `fee` (type: `int`)
- `feeType` (type: `domain.UserFeeType`)
- `balance` (type: `float64`)
- `agentReward` (type: `int`)
- `shiftCount` (type: `int`)
- `payoutAccount` (type: `string`)
- `username` (type: `string`)
- `firstName` (type: `string`)
- `lastName` (type: `string`)
- `wages` (type: `bool`)
- `freeCount` (type: `int`)
- `lease` (type: `bool`)
- `phone` (type: `string`)
- `password` (type: `string`)
- `createdAt` (type: `time.Time`)
- `updatedAt` (type: `time.Time`)
- `cityId` (type: `int`)
- `city` (type: `*domain.City`)

### paymentSystem
- `id` (type: `int`)
- `balance` (type: `float64`)
- `name` (type: `string`)
- `provider` (type: `string`)
- `setting` (type: `string`)
- `userId` (type: `int`)
- `status` (type: `domain.PaymentSystemStatus`)

### transaction
- `id` (type: `int`)
- `externalId` (type: `string`)
- `paymentSystem` (type: `*domain.PaymentSystem`)
- `currency` (type: `string`)
- `amount` (type: `float64`)
- `statusText` (type: `string`)
- `notificationSent` (type: `bool`)
- `status` (type: `domain.TransactionStatus`)
- `userId` (type: `int`)
- `user` (type: `*domain.User`)

### driver
- `id` (type: `int`)
- `name` (type: `string`)
- `phone` (type: `string`)
- `rating` (type: `int`)
- `agentId` (type: `int`)
- `images` (type: `[]string`)
- `status` (type: `domain.DriverStatus`)

### car
- `id` (type: `int`)
- `text` (type: `string`)
- `model` (type: `string`)
- `marque` (type: `string`)
- `year` (type: `int`)
- `fuel` (type: `string`)
- `hasBrand` (type: `bool`)
- `brand` (type: `domain.CarBrand`)
- `price` (type: `int`)
- `freeCount` (type: `int`)
- `transmission` (type: `string`)
- `fleetUserId` (type: `int`)
- `fleet` (type: `*domain.User`)

### offer
- `id` (type: `int`)
- `driverId` (type: `int`)
- `fleetId` (type: `int`)
- `agentId` (type: `int`)
- `comment` (type: `string`)
- `createdAt` (type: `time.Time`)
- `status` (type: `domain.OfferStatus`)

### shift
- `id` (type: `int`)
- `offerID` (type: `int`)
- `shiftNumber` (type: `int`)
- `createdAt` (type: `time.Time`)
- `rating` (type: `int`)
- `comment` (type: `string`)
- `status` (type: `domain.ShiftStatus`)
- `fleetTransactionTransactionId` (type: `int`)
- `fleetTransaction` (type: `*domain.Transaction`)
- `agentTransactionTransactionId` (type: `int`)
- `agentTransaction` (type: `*domain.Transaction`)

### city
- `id` (type: `int`)
- `title` (type: `string`)
- `order` (type: `int`)

### statsItem
- `title` (type: `string`)
- `value` (type: `string`)
- `action` (type: `string`)
- `actionTitle` (type: `string`)

### setting
- `id` (type: `int`)
- `title` (type: `string`)
- `alias` (type: `string`)
- `value` (type: `string`)
- `createdAt` (type: `time.Time`)
- `updatedAt` (type: `time.Time`)

### autocomplete
- `text` (type: `string`)
- `id` (type: `int`)

