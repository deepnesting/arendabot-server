package bank

import (
	"context"

	"github.com/deepnesting/arendabot-server/integration/paymentSystem"

	"go.uber.org/zap"
)

type UsecaseImp struct {
	paymentSystemUc paymentSystem.Usecase

	*zap.Logger
}

func NewUsecase(logger *zap.Logger, paymentSystemUc paymentSystem.Usecase) Usecase {
	return &UsecaseImp{

		paymentSystemUc: paymentSystemUc,

		Logger: logger,
	}
}

func (uc *UsecaseImp) Sync(ctx context.Context) error {
	panic("unimplemented")
}
