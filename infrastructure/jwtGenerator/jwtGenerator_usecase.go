package jwtGenerator

import (
	"context"

	"go.uber.org/zap"
)

type UsecaseImp struct {
	*zap.Logger
}

func NewUsecase(logger *zap.Logger) Usecase {
	return &UsecaseImp{

		Logger: logger,
	}
}

func (uc *UsecaseImp) Generate(ctx context.Context, userId int) (string, error) {
	return uc.repo.GetGenerate(ctx, userId)
}
