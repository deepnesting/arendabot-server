package shift

import (
	"context"

	"github.com/deepnesting/arendabot-server/domain"
	"go.uber.org/zap"
)

type UsecaseImp struct {
	repo *Repo
}

type Config struct{}

func NewUsecase(logger *zap.Logger, cfg Config, repo *Repo) Usecase {
	return &UsecaseImp{
		repo: repo,
	}
}

func (uc *UsecaseImp) ByDriverID(ctx context.Context, driverID int) ([]domain.Shift, error) {
	panic("unimplemented")
}

func (uc *UsecaseImp) SetRating(ctx context.Context, shiftID int, rating int) error {
	panic("unimplemented")
}

func (uc *UsecaseImp) List(ctx context.Context) ([]domain.Shift, error) {
	return uc.repo.ListShift(ctx)
}

func (uc *UsecaseImp) Update(ctx context.Context, shift *domain.Shift) error {
	return uc.repo.UpdateShift(ctx, shift)
}

func (uc *UsecaseImp) Create(ctx context.Context, shift *domain.Shift) (int, error) {
	err := uc.repo.CreateShift(ctx, shift)
	return shift.ID, err
}

func (uc *UsecaseImp) ByOfferID(ctx context.Context, offerID int) ([]domain.Shift, error) {
	return uc.repo.ListShiftByOfferID(ctx, offerID)
}

func (uc *UsecaseImp) ByID(ctx context.Context, id int) (*domain.Shift, error) {
	return uc.repo.GetShiftByID(ctx, id)
}

func (uc *UsecaseImp) RateShift(ctx context.Context, shiftID int, rating int, comment string) error {
	panic("unimplemented")
}

func (uc *UsecaseImp) Delete(ctx context.Context, shiftId int) error {
	return uc.repo.DeleteShift(ctx, shiftId)
}
