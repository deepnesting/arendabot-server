package offer

import (
	"context"
	"log"
	"time"

	"github.com/deepnesting/arendabot-server/domain"
	"go.uber.org/zap"
)

type UsecaseImp struct {
	repo *Repo
}

type Config struct{}

func NewUsecase(logger *zap.Logger, cfg Config, repo *Repo) Usecase {
	return &UsecaseImp{
		repo: repo,
	}
}

func (uc *UsecaseImp) Create(ctx context.Context, offer *domain.Offer) (int, error) {
	offer.CreatedAt = time.Now()
	err := uc.repo.CreateOffer(ctx, offer)
	return offer.ID, err
}

func (uc *UsecaseImp) Update(ctx context.Context, offer *domain.Offer) error {
	return uc.repo.UpdateOffer(ctx, offer)
}

func (uc *UsecaseImp) ByAgentID(ctx context.Context, agentId int) ([]domain.Offer, error) {
	return uc.repo.ListOfferByAgentID(ctx, agentId)
}

func (uc *UsecaseImp) ByDriverID(ctx context.Context, fleetId int) ([]domain.Offer, error) {
	return uc.repo.ListOfferByDriverID(ctx, fleetId)
}

func (uc *UsecaseImp) ByID(ctx context.Context, id int) (*domain.Offer, error) {
	return uc.repo.GetOfferByID(ctx, id)
}

func (uc *UsecaseImp) ByFleetID(ctx context.Context, fleetId int) ([]domain.Offer, error) {
	return uc.repo.ListOfferByFleetID(ctx, fleetId)
}

func (uc *UsecaseImp) List(ctx context.Context) ([]domain.Offer, error) {
	return uc.repo.ListOffer(ctx)
}

func (uc *UsecaseImp) Notify(ctx context.Context) error {
	log.Println("notify")
	return nil
}

func (uc *UsecaseImp) Delete(ctx context.Context, offerId int) error {
	return uc.repo.DeleteOffer(ctx, offerId)
}
