package auth

import (
	"context"
	"fmt"
	"log"

	"github.com/deepnesting/arendabot-server/domain"
	"github.com/deepnesting/arendabot-server/domain/user"
	"github.com/deepnesting/arendabot-server/infrastructure"
	"go.uber.org/zap"
)

type UsecaseImp struct {
	repo *Repo

	userUc user.Usecase

	jwtGeneratorUc infrastructure.JwtGenerator
}
type Config struct{}

func NewUsecase(logger *zap.Logger, cfg Config, repo *Repo, userUc user.Usecase, jwtGeneratorUc infrastructure.JwtGenerator) Usecase {
	return &UsecaseImp{
		repo: repo,

		userUc: userUc,

		jwtGeneratorUc: jwtGeneratorUc,
	}
}

func (uc *UsecaseImp) Login(ctx context.Context, phone string, password string) (string, error) {
	log.Println(phone, password)
	user, err := uc.userUc.ByUsername(ctx, phone)
	if err != nil {
		return "", err
	}
	log.Println(user.Password, password)
	if user.Password != password {
		return "", fmt.Errorf("bad password or login")
	}

	return uc.jwtGeneratorUc.GenerateAccessToken(ctx, user.ID)
}

func (uc *UsecaseImp) SendCode(ctx context.Context) error {

	panic("unimplemented")

}

func (uc *UsecaseImp) Me(ctx context.Context) (*domain.User, error) {
	idFace := ctx.Value("userID")
	if id, ok := idFace.(int); ok {
		return &domain.User{
			ID: id,
		}, nil
	}

	return nil, fmt.Errorf("not logged in")
}
