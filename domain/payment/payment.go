// Code generated by https://github.com/zhuharev/gen DO NOT EDIT.

package payment

import (
	"context"

	"github.com/deepnesting/arendabot-server/domain"
)

type Usecase interface {
	TransactionsForNotification(ctx context.Context) ([]domain.Transaction, error)
	SetTransactionNotificationSent(ctx context.Context, transactionID int) error
	UpdateUserBalances(ctx context.Context) error
	ByID(ctx context.Context, id int) (*domain.PaymentSystem, error)
	List(ctx context.Context) ([]domain.PaymentSystem, error)
	ListByUserID(ctx context.Context, userId int) ([]domain.PaymentSystem, error)
	Create(ctx context.Context, paymentSystem *domain.PaymentSystem) (int, error)
	Update(ctx context.Context, paymentSystem *domain.PaymentSystem) error
	TransferRealMoneyToAgents(ctx context.Context) (string, error)
}
