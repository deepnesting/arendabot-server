package payment

import (
	"context"
	"fmt"
	"log"
	"strings"

	"github.com/deepnesting/arendabot-server/domain"
	"github.com/deepnesting/arendabot-server/infrastructure"
	"github.com/zhuharev/qiwi"
	"go.uber.org/zap"

	"github.com/deepnesting/arendabot-server/integration/paymentSystem"
)

type UsecaseImp struct {
	repo   *Repo
	logger *zap.Logger

	paymentSystemUc paymentSystem.Usecase
}

type Config struct{}

func NewUsecase(logger *zap.Logger, cfg Config, repo *Repo, paymentSystemUc paymentSystem.Usecase) Usecase {
	return &UsecaseImp{
		repo:   repo,
		logger: logger,

		paymentSystemUc: paymentSystemUc,
	}
}

func (uc *UsecaseImp) ByID(ctx context.Context, id int) (*domain.PaymentSystem, error) {
	return uc.repo.GetPaymentSystemByID(ctx, id)
}

func (uc *UsecaseImp) ListByUserID(ctx context.Context, userId int) ([]domain.PaymentSystem, error) {
	return uc.repo.ListPaymentSystemByUserID(ctx, userId)
}

func (uc *UsecaseImp) Create(ctx context.Context, paymentSystem *domain.PaymentSystem) (int, error) {
	err := uc.repo.CreatePaymentSystem(ctx, paymentSystem)
	return paymentSystem.ID, err
}

func (uc *UsecaseImp) Update(ctx context.Context, paymentSystem *domain.PaymentSystem) error {
	return uc.repo.UpdatePaymentSystem(ctx, paymentSystem)
}

func (uc *UsecaseImp) List(ctx context.Context) ([]domain.PaymentSystem, error) {
	return uc.repo.ListPaymentSystem(ctx)
}

// TransferRealMoneyToAgents переводим деньги на кошельки агентов.
// Если кошелёк не привязан, не переводим.
func (uc *UsecaseImp) TransferRealMoneyToAgents(ctx context.Context) (string, error) {
	agents, err := uc.repo.ListUserByRole(ctx, domain.UserRoleAgent)
	if err != nil {
		return "Ошибка!", fmt.Errorf("get agetns from db: %w", err)
	}

	var (
		totalTransfered = 0
		totalAgents     = 0
		skiped          = 0
	)
	for _, agent := range agents {
		if len(agent.PayoutAccount) != 10 {
			uc.logger.Info("skip payout", zap.Int("user", agent.ID), zap.String("payoutAccount", agent.PayoutAccount))
			skiped++
			continue
		}

		amount, err := uc.transferRealMoneyToAgent(ctx, agent)
		if err != nil {
			return fmt.Sprintf("Ошибка вывода агенту %d", agent.ID), err
		}

		totalTransfered += amount
		totalAgents++
	}

	return fmt.Sprintf("Выплачено %d₽. Агентов: %d. Пропущенно (не заполнили кошельки): %d", totalTransfered, totalAgents, skiped), nil
}

func (uc *UsecaseImp) transferRealMoneyToAgent(ctx context.Context, agent domain.User) (int, error) {
	amount := 0
	err := uc.repo.RunInTransaction(ctx, func(ctx context.Context, repo *Repo) error {
		// считаем текущий баланс
		transactions, err := repo.ListTransactionByUserID(ctx, agent.ID)
		if err != nil {
			return fmt.Errorf("list agent transaction: %w", err)
		}

		for _, tx := range transactions {
			if tx.Status != domain.TransactionStatusDone {
				continue
			}
			amount += int(tx.Amount)
		}

		// достаём кошелёк с достаточным балансом
		wallets, err := repo.ListPaymentSystemByStatus(ctx, domain.PaymentSystemStatusActive)
		if err != nil {
			return fmt.Errorf("get active wallets from db: %w", err)
		}

		if len(wallets) == 0 {
			return fmt.Errorf("db does not contain any active wallet")
		}

		var (
			wallet domain.PaymentSystem
			found  bool
		)

		// ищем нужный кошелёк
		for _, w := range wallets {
			// проверяем что на кошельке баланс + 2% больше чем сумма вывода
			if int(w.Balance+(float64(amount)*0.02)) > amount {
				wallet = w
				found = true
				break
			}
		}

		if !found {
			return fmt.Errorf("wallet with corresponding balance not found")
		}

		tx := domain.Transaction{
			UserID: agent.ID,
			Status: domain.TransactionStatusDone,
			Amount: -float64(amount),
		}
		// создаём транзакцию, уменьшающую баланс пользователя
		err = repo.CreateTransaction(ctx, &tx)
		if err != nil {
			return fmt.Errorf("create transaction: %w", err)
		}

		// выводим деньги
		arr := strings.Split(wallet.Setting, ":")
		if len(arr) != 2 {
			err = fmt.Errorf("bad wallet setting: %s", wallet.Setting)
			return err
		}
		account, token := arr[0], arr[1]

		qw := qiwi.New(token, qiwi.Wallet(account))

		log.Printf("send money to user=%d amount=%d", agent.ID, amount)
		pr, err := qw.Payments.Payment(context.TODO(), qiwi.QiwiProviderID, float64(amount), "+7"+agent.PayoutAccount, "")
		if err != nil {
			return fmt.Errorf("qiwi: create payment: %w", err)
		}

		if pr.ID == "" {
			return fmt.Errorf("qiwi: empty remote tx ID")
		}

		tx.ExternalID = pr.ID

		err = repo.UpdateTransaction(ctx, &tx)
		if err != nil {
			return fmt.Errorf("set external tx ID: %w", err)
		}

		return nil
	})
	return amount, err
}

func (uc *UsecaseImp) UpdateUserBalances(ctx context.Context) error {
	agents, err := uc.repo.ListUserByRole(ctx, domain.UserRoleAgent)
	if err != nil {
		return err
	}

	for _, agent := range agents {
		transactions, err := uc.repo.ListTransactionByUserID(ctx, agent.ID)
		if err != nil {
			return err
		}
		var balance float64
		for _, tx := range transactions {
			if tx.Status != domain.TransactionStatusDone {
				continue
			}
			balance += tx.Amount
		}

		agent.Balance = balance
		err = uc.repo.UpdateUserField(ctx, &agent, infrastructure.Field{Name: "Balance", Value: balance})
		if err != nil {
			return err
		}
	}

	return nil
}

func (uc *UsecaseImp) TransactionsForNotification(ctx context.Context) ([]domain.Transaction, error) {
	return uc.repo.ListTransactionByNotificationSent(ctx, false)
}

func (uc *UsecaseImp) SetTransactionNotificationSent(ctx context.Context, transactionID int) error {
	return uc.repo.UpdateTransactionField(ctx, &domain.Transaction{ID: transactionID}, infrastructure.Field{Name: "NotificationSent", Value: true})
}
