package wallet

import (
	"context"

	"github.com/deepnesting/arendabot-server/domain"
	"go.uber.org/zap"
)

type UsecaseImp struct {
	repo *Repo
}

type Config struct{}

func NewUsecase(logger *zap.Logger, cfg Config, repo *Repo) Usecase {
	return &UsecaseImp{
		repo: repo,
	}
}

func (uc *UsecaseImp) UserTransactions(ctx context.Context, userId int) ([]domain.Transaction, error) {
	txns, _ := uc.repo.ListTransactionByUserID(ctx, userId)
	if len(txns) == 0 {
		return nil, nil
	}
	return txns, nil
}

func (uc *UsecaseImp) Transactions(ctx context.Context) ([]domain.Transaction, error) {
	return uc.repo.ListTransaction(ctx)
}

func (uc *UsecaseImp) CreateTransaction(ctx context.Context, transaction *domain.Transaction) (int, error) {
	err := uc.repo.RunInTransaction(ctx, func(ctx context.Context, repo *Repo) error {
		u, err := repo.GetUserByID(ctx, transaction.UserID)
		if err != nil {
			return err
		}

		err = repo.CreateTransaction(ctx, transaction)
		if err != nil {
			return err
		}

		u.Balance += transaction.Amount //FIXME: округление

		err = repo.UpdateUser(ctx, u)
		if err != nil {
			return err
		}

		return nil
	})
	if err != nil {
		return 0, err
	}
	return transaction.ID, nil
}
