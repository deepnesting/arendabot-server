// Code generated by https://github.com/zhuharev/gen DO NOT EDIT.

package wallet

import (
	"context"

	"github.com/deepnesting/arendabot-server/domain"
)

type Usecase interface {
	UserTransactions(ctx context.Context, userId int) ([]domain.Transaction, error)
	Transactions(ctx context.Context) ([]domain.Transaction, error)
	CreateTransaction(ctx context.Context, transaction *domain.Transaction) (int, error)
}
