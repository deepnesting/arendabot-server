package car

import (
	"context"

	"github.com/deepnesting/arendabot-server/domain"
	"go.uber.org/zap"
)

type UsecaseImp struct {
	repo *Repo
}
type Config struct{}

func NewUsecase(logger *zap.Logger, cfg Config, repo *Repo) Usecase {
	return &UsecaseImp{
		repo: repo,
	}
}

func (uc *UsecaseImp) Create(ctx context.Context, car *domain.Car) (int, error) {
	err := uc.repo.CreateCar(ctx, car)
	return car.ID, err
}

func (uc *UsecaseImp) Update(ctx context.Context, car *domain.Car) error {
	return uc.repo.UpdateCar(ctx, car)
}

func (uc *UsecaseImp) ByFleetID(ctx context.Context, fleetId int) ([]domain.Car, error) {
	return uc.repo.ListCarByFleetUserID(ctx, fleetId)
}

func (uc *UsecaseImp) ByID(ctx context.Context, id int) (*domain.Car, error) {
	return uc.repo.GetCarByID(ctx, id)
}

func (uc *UsecaseImp) List(ctx context.Context) ([]domain.Car, error) {
	return uc.repo.ListCar(ctx)
}
