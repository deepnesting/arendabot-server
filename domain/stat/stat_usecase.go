package stat

import (
	"bytes"
	"context"
	"fmt"
	"log"
	"strconv"
	"text/template"

	"github.com/deepnesting/arendabot-server/domain"
	"github.com/zhuharev/tamework"

	botAPI "github.com/go-telegram-bot-api/telegram-bot-api"
	"github.com/robfig/cron/v3"
	"go.uber.org/zap"
)

type UsecaseImp struct {
	*zap.Logger

	repo        *Repo
	telegramBot *botAPI.BotAPI
	cron        *cron.Cron
}

type Config struct {
	TelegramToken string `yaml:"telegramToken"`
}

func NewUsecase(logger *zap.Logger, cfg Config, repo *Repo) Usecase {
	logger.Info("init telegram bot", zap.String("token", cfg.TelegramToken))
	c, err := botAPI.NewBotAPI(cfg.TelegramToken)
	if err != nil {
		panic("init telegram bot" + cfg.TelegramToken + err.Error())
	}
	c.Send(botAPI.NewMessage(102710272, "server started 14"))

	cron := cron.New()

	uc := &UsecaseImp{
		repo:        repo,
		telegramBot: c,
		Logger:      logger,
		cron:        cron,
	}

	// каждое утро в :% * 9 * * *
	cron.AddFunc("0 9 * * *", uc.sendCarsNotify)
	cron.AddFunc("0 14 * * *", uc.sendShiftNotify)
	cron.AddFunc("0 16 * * *", uc.sendShiftNotify)

	cron.Start()

	return uc
}

func (uc *UsecaseImp) List(ctx context.Context) ([]domain.StatsItem, error) {
	fns := []func(ctx context.Context) (domain.StatsItem, error){
		uc.totalBalance,
		uc.totalObligations,
		carsNotificationAction,
		shiftNotificationAction,
	}
	var res []domain.StatsItem
	for _, fn := range fns {
		i, err := fn(ctx)
		if err != nil {
			return nil, err
		}
		res = append(res, i)
	}
	return res, nil
}

func carsNotificationAction(ctx context.Context) (domain.StatsItem, error) {
	return domain.StatsItem{
		Title:       "Отправить уведомления о машинах",
		Action:      "stats.sendCarsNotify",
		ActionTitle: "Отправить",
	}, nil
}

func shiftNotificationAction(ctx context.Context) (domain.StatsItem, error) {
	return domain.StatsItem{
		Title:       "Отправить уведомления о сменах",
		Action:      "stats.sendShiftNotify",
		ActionTitle: "Отправить",
	}, nil
}

func (uc *UsecaseImp) totalBalance(ctx context.Context) (domain.StatsItem, error) {
	activeWallets, err := uc.repo.ListPaymentSystemByStatus(ctx, domain.PaymentSystemStatusActive)
	if err != nil {
		return domain.StatsItem{}, err
	}
	var b int
	for _, w := range activeWallets {
		b += int(w.Balance)
	}
	return domain.StatsItem{
		Title: "Общий баланс",
		Value: strconv.Itoa(b) + "₽",
	}, nil
}

// totalObligations сколько мы должны агентам
func (uc *UsecaseImp) totalObligations(ctx context.Context) (domain.StatsItem, error) {
	agents, err := uc.repo.ListUserByRole(ctx, domain.UserRoleAgent)
	if err != nil {
		return domain.StatsItem{}, err
	}

	var b float64
	for _, a := range agents {
		txs, err := uc.repo.ListTransactionByUserID(ctx, a.ID)
		if err != nil {
			return domain.StatsItem{}, err
		}

		for _, tx := range txs {
			if tx.Status != domain.TransactionStatusDone {
				continue
			}
			b += tx.Amount
		}
	}

	return domain.StatsItem{
		Title:       "Сумма выплат агентам на понедельник",
		Value:       strconv.Itoa(int(b)) + "₽",
		Action:      "payments.transferRealMoneyToAgents",
		ActionTitle: "Выплатить сейчас",
	}, nil
}

func (uc *UsecaseImp) sendCarsNotify() {
	ctx := context.Background()
	result, err := uc.SendCarsNotify(ctx)
	if err != nil {
		uc.telegramBot.Send(botAPI.NewMessage(102710272, fmt.Sprintf("Ошибка отправки уведомлений о машинах: %s", err)))
		return
	}
	uc.telegramBot.Send(botAPI.NewMessage(102710272, fmt.Sprintf("Уведомление о машинах отправлено: %s", result)))
}

func (uc *UsecaseImp) sendShiftNotify() {
	ctx := context.Background()
	result, err := uc.SendShiftNotify(ctx)
	if err != nil {
		uc.telegramBot.Send(botAPI.NewMessage(102710272, fmt.Sprintf("Ошибка отправки уведомлений о сменах: %s", err)))
		return
	}
	uc.telegramBot.Send(botAPI.NewMessage(102710272, fmt.Sprintf("Уведомление о сменах отправлено: %s", result)))
}

func (uc *UsecaseImp) SendCarsNotify(ctx context.Context) (string, error) {
	fleets, err := uc.repo.ListUserByRole(ctx, domain.UserRoleFleet)
	if err != nil {
		return "Ошибка!", err
	}
	for _, fleet := range fleets {
		cars, err := uc.repo.ListCarByFleetUserID(ctx, fleet.ID)
		if err != nil {
			return "", err
		}
		for _, car := range cars {
			msg := botAPI.NewMessage(int64(fleet.ID), fmt.Sprintf("Уточните сколько машин в наличии по автомобилю %s. Свободно: %d", car.Text, car.FreeCount))
			kb := tamework.NewKeyboard(nil).AddCallbackButton("Изменить", "changeFreeCar:"+strconv.Itoa(car.ID)).AddCallbackButton("").AddCallbackButton("Подтвердить", "noop")
			msg.ReplyMarkup = kb.Markup()
			_, err := uc.telegramBot.Send(msg)
			if err != nil {
				return "", err
			}
		}
	}
	return "Отправлено", nil
}

// SendShiftNotify отправляет уведомление парку по активным сменам
func (uc *UsecaseImp) SendShiftNotify(ctx context.Context) (string, error) {
	shifts, err := uc.repo.ListShiftByStatus(ctx, domain.ShiftStatusActive)
	if err != nil {
		return "", err
	}
	count := 0
	for _, shift := range shifts {
		offer, err := uc.repo.GetOfferByID(ctx, shift.OfferID)
		if err != nil {
			return "", err
		}

		driver, err := uc.repo.GetDriverByID(ctx, offer.DriverID)
		if err != nil {
			return "", err
		}

		agent, err := uc.repo.GetUserByID(ctx, offer.AgentID)
		if err != nil {
			return "", err
		}

		text := `Добрый день! Не забудьте нажать кнопку "закрыть смену", если водитель выполнил очередную смену. После нажатия кнопки с Вашего баланса произойдет автоматическое списание.`
		msg1 := botAPI.NewMessage(int64(offer.FleetID), text)
		_, err = uc.telegramBot.Send(msg1)
		if err != nil {
			return "", err
		}

		text, _ = renderShift(shift, *offer, *driver, *agent)

		log.Printf("send shift notify=%d", shift.ID)

		msg := botAPI.NewMessage(int64(offer.FleetID), text)
		kb := tamework.NewKeyboard(nil).AddCallbackButton("Закрыть смену", "closeShift:"+strconv.Itoa(shift.ID))
		msg.ReplyMarkup = kb.Markup()
		msg.ParseMode = botAPI.ModeMarkdown
		_, err = uc.telegramBot.Send(msg)
		if err != nil {
			return "", err
		}
		count++
	}
	return fmt.Sprintf("Отправлено. Сообщений: %d", count), nil
}

const shiftTmpl = `Смена {{ .shift.ShiftNumber }}
Водитель: {{ .driver.Name }} ({{ .driver.Phone }})
Агент: {{ .agent.Name }} ({{ .agent.Username }})
Дата {{ .shift.CreatedAt.Format "02.01.2006" }}
Статус: {{ .shift.StatusText }}
`

func renderShift(shift domain.Shift, offer domain.Offer, driver domain.Driver, agent domain.User) (string, error) {
	return execTemplate(shiftTmpl, map[string]interface{}{"shift": shift, "offer": offer, "driver": driver, "agent": agent})
}

func execTemplate(tmpl string, data map[string]interface{}) (string, error) {
	tpl, err := template.New("").Funcs(template.FuncMap{}).Parse(tmpl)
	if err != nil {
		return "", err
	}
	var buf bytes.Buffer
	err = tpl.Execute(&buf, data)
	return buf.String(), err
}
