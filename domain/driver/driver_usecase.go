package driver

import (
	"context"

	"github.com/deepnesting/arendabot-server/domain"
	"go.uber.org/zap"
)

type UsecaseImp struct {
	repo *Repo
}

type Config struct{}

func NewUsecase(logger *zap.Logger, cfg Config, repo *Repo) Usecase {
	return &UsecaseImp{
		repo: repo,
	}
}

func (uc *UsecaseImp) Create(ctx context.Context, driver *domain.Driver) (int, error) {
	err := uc.repo.CreateDriver(ctx, driver)
	return driver.ID, err
}

func (uc *UsecaseImp) Update(ctx context.Context, driver *domain.Driver) error {
	return uc.repo.UpdateDriver(ctx, driver)
}

func (uc *UsecaseImp) ByAgentID(ctx context.Context, agentId int) ([]domain.Driver, error) {
	drivers, err := uc.repo.ListDriverByAgentID(ctx, agentId)
	if err != nil {
		return nil, err
	}
	var res []domain.Driver
	for _, d := range drivers {
		if d.Status == domain.DriverStatusDisabled {
			continue
		}
		res = append(res, d)
	}
	return res, nil
}

func (uc *UsecaseImp) ByID(ctx context.Context, id int) (*domain.Driver, error) {
	return uc.repo.GetDriverByID(ctx, id)
}

func (uc *UsecaseImp) List(ctx context.Context) ([]domain.Driver, error) {
	return uc.repo.ListDriver(ctx)
}
