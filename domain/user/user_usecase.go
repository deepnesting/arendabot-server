package user

import (
	"context"
	"fmt"
	"log"
	"strconv"
	"strings"

	"github.com/deepnesting/arendabot-server/domain"
	"go.uber.org/zap"
)

type UsecaseImp struct {
	repo *Repo
}

type Config struct{}

func NewUsecase(logger *zap.Logger, cfg Config, repo *Repo) Usecase {
	repo.afterReadUser = fillUser
	repo.afterReadListUser = fillUsers
	return &UsecaseImp{
		repo: repo,
	}
}

func (uc *UsecaseImp) ByID(ctx context.Context, id int) (*domain.User, error) {

	return uc.repo.GetUserByID(ctx, id)

}

func (uc *UsecaseImp) ByPhone(ctx context.Context, phone string) (*domain.User, error) {

	return uc.repo.GetUserByPhone(ctx, phone)

}

func (uc *UsecaseImp) Update(ctx context.Context, user *domain.User) error {
	return uc.repo.Update(ctx, user)
}

func (uc *UsecaseImp) Create(ctx context.Context, user *domain.User) (int, error) {
	log.Println("create user request")
	err := uc.repo.CreateUser(ctx, user)
	if err != nil {
		return 0, fmt.Errorf("create user repo err: %w", err)
	}
	return user.ID, err
}

func (uc *UsecaseImp) ByUsername(ctx context.Context, username string) (*domain.User, error) {
	return uc.repo.GetUserByUsername(ctx, username)
}

func (uc *UsecaseImp) List(ctx context.Context) ([]domain.User, error) {
	// fill cities
	cities, err := uc.repo.ListCity(ctx)
	if err != nil {
		return nil, err
	}

	users, err := uc.repo.ListUser(ctx)
	if err != nil {
		return nil, err
	}

	for i, u := range users {
		for _, c := range cities {
			if u.CityID == c.ID {
				users[i].City = c
			}
		}
	}
	return users, nil
}

func (uc *UsecaseImp) ByRole(ctx context.Context, role int) ([]domain.User, error) {
	return uc.repo.ListUserByRole(ctx, domain.UserRole(role))
}

func (uc *UsecaseImp) Cities(ctx context.Context) ([]domain.City, error) {
	return uc.repo.ListCity(ctx)
}

func (uc *UsecaseImp) AddCity(ctx context.Context, city *domain.City) (int, error) {
	err := uc.repo.CreateCity(ctx, city)
	if err != nil {
		return 0, err
	}
	return city.ID, nil
}

func (uc *UsecaseImp) UpdateCity(ctx context.Context, city *domain.City) error {
	return uc.repo.UpdateCity(ctx, city)
}

func (uc *UsecaseImp) DeleteCity(ctx context.Context, id int) error {
	return uc.repo.DeleteCity(ctx, id)
}

func (uc *UsecaseImp) ByCityID(ctx context.Context, cityId int) ([]domain.User, error) {
	return uc.repo.ListUserByCityID(ctx, cityId)
}

func (uc *UsecaseImp) AutocompleteCity(ctx context.Context, q string) ([]domain.Autocomplete, error) {
	cities, err := uc.repo.ListCity(ctx)
	if err != nil {
		return nil, err
	}
	var res []domain.Autocomplete
	for _, cat := range cities {
		if strings.Contains(strings.ToLower(cat.Title), strings.ToLower(q)) || strings.Contains(strconv.Itoa(cat.ID), q) {
			res = append(res, domain.Autocomplete{
				Text: cat.Title,
				ID:   cat.ID,
			})
		}
	}
	return res, nil
}

func (uc *UsecaseImp) SearchFleets(ctx context.Context, cityId int, wages bool, lease bool, freeCars bool) error {
	panic("unimplemented")
}
