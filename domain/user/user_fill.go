package user

import (
	"context"

	"github.com/deepnesting/arendabot-server/domain"
)

func fillUser(ctx context.Context, repo *Repo, u *domain.User) error {
	// количество свободных машин в парке
	if u.Role == domain.UserRoleFleet {
		cars, err := repo.ListCarByFleetUserID(ctx, u.ID)
		if err != nil {
			return err
		}
		var totalFree int
		for _, c := range cars {
			totalFree += c.FreeCount
		}
		u.FreeCount = totalFree
	}
	return nil
}

func fillUsers(ctx context.Context, repo *Repo, users []domain.User) ([]domain.User, error) {
	allCars, err := repo.ListCar(ctx)
	if err != nil {
		return users, nil
	}
	for i, u := range users {
		var freeCount int
		for _, c := range allCars {
			if c.FleetUserID == u.ID {
				freeCount += c.FreeCount
			}
		}
		users[i].FreeCount = freeCount
	}
	return users, nil
}
