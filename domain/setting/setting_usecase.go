package setting

import (
	"context"

	"github.com/deepnesting/arendabot-server/domain"

	"go.uber.org/zap"
)

type UsecaseImp struct {
	repo *Repo

	*zap.Logger
}

type Config struct {
}

func NewUsecase(logger *zap.Logger, _ Config, repo *Repo) Usecase {
	return &UsecaseImp{
		repo: repo,

		Logger: logger,
	}
}

func (uc *UsecaseImp) List(ctx context.Context) ([]domain.Setting, error) {
	return uc.repo.ListSetting(ctx)
}

func (uc *UsecaseImp) Update(ctx context.Context, setting *domain.Setting) error {
	return uc.repo.UpdateSetting(ctx, setting)
}

func (uc *UsecaseImp) Create(ctx context.Context, setting *domain.Setting) (int, error) {
	err := uc.repo.CreateSetting(ctx, setting)
	return setting.ID, err
}
